from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail, Message
from flask_login import LoginManager
from flask_bcrypt import Bcrypt
from flask_moment import Moment
from config import config
import os

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'
login_manager.login_message_category = "info"
bcrypt = Bcrypt()
moment = Moment()
mail = Mail()
db = SQLAlchemy()

basedir = os.path.abspath(os.path.dirname(__file__))

def create_app(config_name):
    app = Flask(__name__, static_url_path='/static')
    app.config.from_object(config[config_name])
    app.config['SQLALCHEMY_DATABASE_URI'] = \
                    'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    app.config['UPLOAD_FOLDER'] = os.getcwd() + '/myinvoices/static/pdfs/'
    config[config_name].init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)
    moment.init_app(app)
    db.init_app(app)
    bcrypt.init_app(app)
    from .main import main as main_blueprint
    from .auth import auth as auth_blueprint
    app.register_blueprint(main_blueprint)
    app.register_blueprint(auth_blueprint)
    return app


if __name__ == '__main__':
    app.run()
