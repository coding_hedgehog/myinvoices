# MyInvoices

Website designed to help builders and traders keep PDF invoices organized allowing
for keyword search.

This website allowed searching for specific invoice in the matter of few seconds instead of
looking through pile of emails within inbox.

technologies used:  HTML5 & CSS, MySQL, Flask, Bulma CSS
